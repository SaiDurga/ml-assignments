def map1(f):
    def mapped(lst):
        result = []
        for e in lst:
            result.append(f(e))
        return result
    return mapped


def inc(n):
    return n+1


map1(inc)([1,2,3,4,5])
