def concat(lst1,lst2):
    result = lst1
    for e in lst2:
        result.append(e)
    return result

def flatten(lst1,lst2):
    res = concat(lst1,lst2)
    result =[]
    for e in res:
        if type(e) == list:
            for i in e:
                result.append(i)
        else:
            result.append(e)
    return result
