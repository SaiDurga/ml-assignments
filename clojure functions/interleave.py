def interleave(lst1,lst2):
    l1 = len(lst1)
    l2 = len(lst2)
    if l1<l2 :
        l = l1
    else:
        l = l2
    result = []
    for i in range(0,l):
        result.append(lst1[i])
        result.append(lst2[i])
    return result

interleave([1,2,3],[4,5,6])
