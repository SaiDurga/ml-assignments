a = 7
print(a)

## o/p: 7

b = a
print(b)

## o/p:7

type(a)
## o/p:int

type(b)
## o/p:int

type(58)
## o/p:int

type(68.3)
## o/p:float

type([1,2,3,4])
## o/p:list

type('saidurga')
## o/p:str

7/2
## o/p:3.5

7//2
## o/p:3

#===============================================================================
##numbers

123
## o/p:123

+123
## o/p:123

-123
## o/p:-123

4+5
## o/p:9

4-5
## o/p:-1

4*5
## o/p:20

5 + 9 + 3
## o/p:17

4 + 3 - 2 - 1 + 6
## o/p:10

1*2*3*4*5
## o/p:120

a =30
a
## o/p:30

a = a+2
a
## o/p:32

a-2
## o/p:30

a
## o/p:32

a += 2
a
## o/p:34

a -= 4
a
## o/p:30

a *= 2
a
## o/p:120

a /= 4
a
## o/p:30.0

b = 56.8
b //= 2
b
## o/p:28.0

20%3
## o/p:2

divmod(9,5)
## o/p:(1, 4)

divmod(20,3)
## o/p:(6, 2)

divmod(20,4)
## o/p:(5, 0)

#==================================================================

##Precedence

2+5*4
## o/p:22

2+3*4/2
## o/p:8.0

#=================================================================

##bases

10
## o/p:10

0b10
## o/p:2

0o10
## o/p:8

0x10
## o/p:16
##================================================================

##Type Conversions

int(True)
## o/p:1

int(False)
## o/p:0

int(23.54)
## o/p:23

int(-45)
## o/p:-45

int('a')

##---------------------------------------------------------------------------
##ValueError                                Traceback (most recent call last)
##<ipython-input-77-b3c3f4515dd4> in <module>()
##----> 1 int('a')

##ValueError: invalid literal for int() with base 10: 'a'

int('23.54')

#---------------------------------------------------------------------------
#ValueError                                Traceback (most recent call last)
#<ipython-input-78-215c150b1fbb> in <module>()
#----> 1 int('23.54')

#ValueError: invalid literal for int() with base 10: '23.54'

3+6.3
## o/p:9.3

True+2
## o/p::3

False+4.5
## o/p:4.5

#===================================================================================

##Floats

float(True)
## o/p:1.0

float(False)
## o/p:0.0

float(23)
## o/p:23.0

float('23')
## o/p:23.0

float('-32')
## o/p:32.0

​##===============================================================================================
​
	##Strings

poem2 = '''I do not like thee, Doctor Fell.
The reason why, I cannot tell.
But this I know, and know full well:
I do not like thee, Doctor Fell.'''

print(poem2)

##I do not like thee, Doctor Fell.
##The reason why, I cannot tell.
##But this I know, and know full well:
##I do not like thee, Doctor Fell.

poem2

##'I do not like thee, Doctor Fell.\nThe reason why, I cannot tell.\nBut this I know, and know full well:\nI do not like thee, Doctor Fell.'

print(99, 'bottles', 'would be enough.')

##99 bottles would be enough.

str(98.6)
## o/p: '98.6'

str(1.0e4)
## o/p: '10000.0'

str(True)
## o/p:  'True'

print('\tabc')
## o/p: 	abc

print('a\tbc')
## o/p:a	bc

print('ab\tc')
## o/p:ab	c

print('abc\t')
## o/p: abc	

'Release the kraken! ' + 'At once!'
## o/p: 'Release the kraken! At once!'

"My word! " "A gentleman caller!"
## o/p:	'My word! A gentleman caller!'

a = 'Duck.'
b=a
c = 'Grey Duck'
a+b+c
## o/p: 'Duck.Duck.Grey Duck'

print(a,b,c)
## o/p: Duck. Duck. Grey Duck

start = 'Na ' * 4 + '\n'
print(start)
## o/p:	Na Na Na Na 

middle = 'Hey ' * 3 + '\n'

end = 'Goodbye.'
print(start + start + middle + end)

## o/p: Na Na Na Na 
##	Na Na Na Na 
##	Hey Hey Hey 
##	Goodbye.

letters = 'abcdefghijklmnopqrstuvwxyz'
letters[0]
## o/p: 'a'

letters[1]
## o/p:'b'

letters[-1]
## o/p:'z'

letters[5]
## o/p:'f'

name = 'Henny'
name
## o/p:'Henny'

name.replace('H', 'P')

## o/p:	'Penny'

'P' + name[1:]
## o/p:'Penny'

letters = 'abcdefghijklmnopqrstuvwxyz'

letters[:]
## o/p:	'abcdefghijklmnopqrstuvwxyz'

letters[20:]
## o/p:	'uvwxyz'

letters[10:]
## o/p:'klmnopqrstuvwxyz'

letters[12:15]
## o/p: 'mno'

letters[-3:]
## o/p:'xyz'

letters[18:-3]
## o/p:'stuvw'

letters[-6:-2]
## o/p:'uvwx'

letters[::7]
## o/p:'ahov'

letters[4:20:3]
## o/p:'ehknqt'

letters[19::4]
## o/p:	'tx'

letters[:21:5]
## o/p:'afkpu'

letters[-1::-1]
## o/p:'zyxwvutsrqponmlkjihgfedcba'

letters[::-1]
## o/p:'zyxwvutsrqponmlkjihgfedcba'

letters[-50:]
## o/p:'abcdefghijklmnopqrstuvwxyz'

letters[-51:-50]
## o/p: ''

len(letters)
## o/p:26

todos = 'get gloves,get mask,give cat vitamins,call ambulance'
todos.split(',')
## o/p:['get gloves', 'get mask', 'give cat vitamins', 'call ambulance']

todos.split()
## o/p:['get', 'gloves,get', 'mask,give', 'cat', 'vitamins,call', 'ambulance']

crypto_list = ['Yeti', 'Bigfoot', 'Loch Ness Monster']
crypto_string = ', '.join(crypto_list)
crypto_string
## o/p:'Yeti, Bigfoot, Loch Ness Monster'

print('Found and signing book deals:', crypto_string)
## o/p:Found and signing book deals: Yeti, Bigfoot, Loch Ness Monster

poem = '''All that doth flow we cannot liquid name
	Or else would fire and water be the same;
	But that is liquid which is moist and wet
	Fire that property can never get.
	Then 'tis not cold that doth the fire put out
	But 'tis the wet that makes it die, no doubt.'''

poem[:13]
## o/p:'All that doth'

poem[:78]
## o/p:'All that doth flow we cannot liquid name\nOr else would fire and water be the s'

len(poem)
## o/p:250

poem.startswith('All')
## o/p:True

poem.startswith('yes')
## o/p:False

poem.endswith('That\'s all, folks!')
## o/p:False

poem.endswith('no doubt.')
## o/p:True

word = 'the'
poem.find(word)
## o/p:73

poem.rfind(word)
## o/p:214

poem.count(word)
## o/p:3

poem.isalnum()
## o/p:False

setup = 'a duck goes into a bar...'
setup.strip('.')
## o/p:'a duck goes into a bar'

setup.capitalize()
## o/p:'A duck goes into a bar...'

setup.title()
## o/p:'A Duck Goes Into A Bar...'

setup.upper()
## o/p:'A DUCK GOES INTO A BAR...'

setup.lower()
## o/p:'a duck goes into a bar...'

setup.swapcase()
## o/p:'A DUCK GOES INTO A BAR...'

setup.center(30)
## o/p:'  a duck goes into a bar...   '

setup.ljust(30)
## o/p:'a duck goes into a bar...     '

setup.rjust(60)
## o/p:'                                   a duck goes into a bar...'

setup.replace('duck', 'marmoset')
## o/p:'a marmoset goes into a bar...'

setup.replace('a ', 'a famous ', 100)
## o/p:'a famous duck goes into a famous bar...'

setup.replace('a', 'a famous ', 100)
## o/p:'a famous  duck goes into a famous  ba famous r...'


