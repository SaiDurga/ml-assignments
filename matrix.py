def sum_array(lst):
        sum = 0
        for i in range(0, len(lst)):
            sum = sum + lst[i]
        return sum

class Matrix():
    def __init__(self,lst1,lst2):
        self.lst1 = lst1
        self.lst2 = lst2
        
    def matrix_add(self):
        m1 = len(self.lst1)
        n1 = len(self.lst1[0])
        m2 = len(self.lst2)
        n2 = len(self.lst2[0])
        res = []
        if m1 == m2 and n1 == n2 :
            for i in range(0,m1):
                res1 = []
                for j in range(0,n1):
                    res1.append(self.lst1[i][j]+self.lst2[i][j])
                res.append(res1)
        else:
            print("matrix addition not possible")
        return res

    def matrix_sub(self):
        m1 = len(self.lst1)
        n1 = len(self.lst1[0])
        m2 = len(self.lst2)
        n2 = len(self.lst2[0])
        res = []
        if m1 == m2 and n1 == n2:
            for i in range(0,m1):
                res1 = []
                for j in range(0,n1):
                    res1.append(self.lst1[i][j]-self.lst2[i][j])
                res.append(res1)
        else:
            print("matrix subtraction not possible")
        return res

    def matrix_mul(self):
        m1 = len(self.lst1)
        n1 = len(self.lst1[0])
        m2 = len(self.lst2)
        n2 = len(self.lst2[0])
        res = []
        if n1 == m2 :
            for i in range(0,m1):
                res1 = []
                for j in range(0,n2):
                    res2 = []
                    for k in range(0,m1):
                        res2.append(self.lst1[i][k]*self.lst2[k][j])
                    res1.append(sum_array(res2))
                res.append(res1)
        else:
            print("matrix multiplication not possible")
        return res

    def matrix_transpose(self):
        m = len(self.lst1)
        n = len(self.lst1[0])
        res = []
        for i in range(0,m):
            res1 = []
            for j in range(0,n):
                res1.append(self.lst1[j][i])
            res.append(res1)
        return res
mat = Matrix([[1,2],[3,4]],[[1,2],[3,4]])
print(mat.matrix_mul())
print(mat.matrix_transpose())
